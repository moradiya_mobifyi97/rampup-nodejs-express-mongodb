const mongoose = require('mongoose');
const Joi = require('joi');

const User = mongoose.model("user", mongoose.Schema({
    name:{
        type:String,
        require:true,
        minlength:3,
        maxlength:255
    },
    email:{
        type:String,
        require:true,
        minlength:3,
        maxlength:255,
        unique:true
    },
    password:{
        type:String,
        require:true,
        minlength:3,
        maxlength:255
    },
    isAdmin:Boolean
}));

function ValidityUser (user){

    const schema = {
        name: Joi.string().min(5).max(50).required(),
        email: Joi.string().min(5).max(50).required().email(),
        password: Joi.string().min(5).max(255).required()
    };
    return Joi.validate(user, schema)

}

exports.User = User;
exports.validate = ValidityUser;