const _ = require('lodash');
const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
const {User} = require('../models/user.model');
const express = require('express');
const router = express.Router();
const Joi = require('joi');
const jwt = require("jsonwebtoken");
const config = require('config');

router.post('/', async(req, res) => {
    const {error} = validate(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    let user = await User.findOne({email: req.body.email});
    if(!user) return res.status(400).send("Invaild Email and passwoer");
    
    const validPassword = await bcrypt.compare(req.body.password, user.password)
    if(!validPassword) return res.status(400).send("Invalid email and password");
    const token = jwt.sign({_id:user.id,isAdmin:user.isAdmin},'secret')
    res.header('x-auth-token', token).send(token);
})

function validate (req){
    const schema = {
        email: Joi.string().min(5).max(50).required().email(),
        password: Joi.string().min(5).max(255).required()
    };
    return Joi.validate(req, schema)
}
module.exports = router;