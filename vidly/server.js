const express = require("express");
const app = express();
const mongoose = require('mongoose');
const router = require('./startup/router')(app)

mongoose.connect("mongodb://localhost:27017/vidly",{useNewUrlParser:true},(err) => {
    if(!err){
        console.log("Connectd to mongodb")
    }else{
        console.log('could not connected to Mongodb');
    }
});

const port = process.env.PORT || 3000;

app.listen(port,() => {
    console.log(`Listing on port ${port}`);
})