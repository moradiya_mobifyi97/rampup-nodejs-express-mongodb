const mongoose = require('mongoose');
const Joi = require('joi');

const Customer = mongoose.model("customer", mongoose.Schema({
    name:{
        type:String,
        require:true,
        minlength:3,
        maxlength:255
    },
    isGold:{
        type:String,
        require:true,
        minlength:3,
        maxlength:255
    },
    phone:{
        type:String,
        require:true,
        minlength:3,
        maxlength:255
    }
}));

function ValidityCustomer (genre){

    const schema = {
        name: Joi.string().min(5).max(50).required(),
        name: Joi.string().min(5).max(50).required(),
        isGold:Joi.boolean()
    };
    console.log("schema", schema)
    return Joi.validate(genre, schema)

}

exports.Customer = Customer;
exports.validate = ValidityCustomer;