const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const Joi = require('joi');
const checkAuth = require('../middleware/auth');
const checkAdmin = require("../middleware/admin");

const Genres = mongoose.model("genres", mongoose.Schema({
    name:{
        type:String,
        require:true,
        minlength:3,
        maxlength:255
    }
}));

router.get('/', async(req, res) => {
    const genres = await Genres.find().sort('name');
    res.send(genres);
});

router.post('/', checkAuth, async(req, res) => {
    const {error} = ValidityGenre(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    const genres = new Genres({
        name:req.body.name
    })
    const genre = await genres.save();
    res.send(genre);
})

router.put('/:id', async(req, res) => {
    const {error} = ValidityGenre(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    const genres = await Genres.findByIdAndUpdate(req.params.id, {name:req.body.name}, {new:true});
    if(!genres) return res.status(404).send('Then genre with the given ID');

    res.send(genres);
})

router.delete('/:id', [checkAuth, checkAdmin], async(req, res) => {
    const genres = await Genres.findByIdAndRemove(req.params.id);
    if(!genres) return res.status(404).send('Then genre with the given ID');

    res.send(genres);
})

router.get('/:id', async (req, res) => {

    const genres = await Genres.findById(req.params.id);
    if(!genres) return res.status(404).send('Then genre with the given ID');

    res.send(genres);

})

function ValidityGenre (genre){
    const schema = {
        name: Joi.string().min(3).required()
    };
    console.log("schema", schema)
    return Joi.validate(genre, schema)

}

module.exports = router;
